﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DKstudent.Models;

namespace DKstudent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SinhViensController : ControllerBase
    {
        private readonly StudentContext _context;

        public SinhViensController(StudentContext context)
        {
            _context = context;
        }

        // GET: api/SinhViens
        [HttpGet]
        public ActionResult<IEnumerable<SinhVien>> GetSinhViens()
        {
          if (_context.SinhViens == null)
          {
              return NotFound();
          }
            return _context.SinhViens.ToList();
        }

        // GET: api/SinhViens/5
        [HttpGet("{id}")]
        public ActionResult<SinhVien> GetSinhVien(int id)
        {
          if (_context.SinhViens == null)
          {
              return NotFound();
          }
            var sinhVien = _context.SinhViens.Find(id);

            if (sinhVien == null)
            {
                return NotFound();
            }

            return sinhVien;
        }

        // PUT: api/SinhViens/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public IActionResult PutSinhVien(int id, SinhVien sinhVien)
        {
            if (id != sinhVien.Id)
            {
                return BadRequest();
            }

            _context.Entry(sinhVien).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SinhVienExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SinhViens
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult<SinhVien> PostSinhVien(SinhVien sinhVien)
        {
          if (_context.SinhViens == null)
          {
              return Problem("Entity set 'StudentContext.SinhViens'  is null.");
          }
            _context.SinhViens.Add(sinhVien);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SinhVienExists(sinhVien.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSinhVien", new { id = sinhVien.Id }, sinhVien);
        }

        // DELETE: api/SinhViens/5
        [HttpDelete("{id}")]
        public IActionResult DeleteSinhVien(int id)
        {
            if (_context.SinhViens == null)
            {
                return NotFound();
            }
            var sinhVien = _context.SinhViens.Find(id);
            if (sinhVien == null)
            {
                return NotFound();
            }

            _context.SinhViens.Remove(sinhVien);
            _context.SaveChanges();

            return NoContent();
        }

        private bool SinhVienExists(int id)
        {
            return (_context.SinhViens?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
