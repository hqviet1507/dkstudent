﻿using System;
using System.Collections.Generic;

namespace DKstudent.Models
{
    public partial class SinhVien
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int Age { get; set; }
        public string PhoneNumber { get; set; } = null!;
    }
}
